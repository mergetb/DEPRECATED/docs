= Merge Driver Developers Guide
:toc: left
:toclevels: 4
:icons: font
:sectnums:
:sectanchors:
:sectlinks:
:source-highlighter: coderay

== Driver Architecture

The driver architecture is designed to remove complexity out of the core
of Merge.  It follows from operating system design that drivers do not belong
in the operating system, but as tools for the operating system to carry out
its goal.  To this extent, drivers are self contained modules that can be
loaded by a site commander to implement the functionality of a resource.

The lifecycle of a driver in the context of Merge is fairly mundane.

A site, wether it is a university, corporation, or other entity, will have a
single commander that acts as the site's policy enforcer, and resource
negotiator.  The site owner has resources they would like to provide to the
community for experimentation.  The site owner must ensure that the resources
have been registered with the commander, and that there is software (drivers)
that delegate the tasks and actions of a resource on behalf of a user.  The
mapping of resource, driver is installed in the commander for when the Merge
architecture needs to allocate and use the resource. 

From the experimenter's side, an experiment is created.  The experiment, goes through
a discovery process to find the resources necessary to the experiment.  Once
those resources are mapped to the experiment in the realization phase.  The
Merge core sends a protobuf message to the site owners of each respective
resource, allocating the resource from the commander for the experiment.

Now the resources have been allocated to the experiment by both Merge and the
sites, the experiment can be implemented.  Through materialization agents
in the Merge core, protobuf messages are exchanged through the site commanders,
which keep a mapping of (resource_id, driver). The commander forwards the
message from the materialization agent to the appropriate driver controlling
the resource.

That may seem like a lot of work, but it may not be too much work. Merge is
designed to help foster the cybersecurity experimentation community. Drivers,
or other components may have already been developed by others to assist in
the development of drivers in cases where site owners don't want to re-implement
an IPMI driver and are willing to trust another developers implementation.

== The Driver State Machine

The driver state machine has 4 states: on, off, setup, and configured.  The
on and off states are self explanatory.  In the setup state, the resource has
been setup with the settings the resource will need for the experiment.  These
can be though of as settings such as operating system or firmware.

[.text-center]
image::driver.png[alt=Merge's driver state machine, width=50%, height=50%]

From the setup state, the resource can be configured, in this state the resource
is assumed to have the necessary software, hardware.  So configure could be
installing gcc (which could be in the setup stage); building, or running code
on the resource.  During the configured state, it is assumed that the experiment
will be run.

Each state allows for a transition to off (off transition), and to on (reset,
with the exception of off).  Through these four states, a resource can be
easily controlled through a simple state machine, while allowing complex
configuration in setup and configured states.  The driver is also capable of
exposing resource specific features through this state machine.


== The Driver API

The functionality which the driver must implement for minimum specification
(https://github.com/ceftb/site/blob/master/api/driver.proto[Driver Protobuf^])
with their message types
(https://github.com/ceftb/site/blob/master/api/shared.proto[Shared Protobuf^]).

Examples of Driver APIs can be found on the
https://github.com/ceftb/site/tree/master/drivers[Drivers^] GitHub page.  In
documentation we will use examples of the Android python3 drivers.

[source,protobuf]
-----------------
# driver.proto
service Device {
  rpc HealthCheck(HealthCheckRequest) returns (HealthCheckResponse);
  rpc Setup(SetupRequest) returns (SetupResponse);
  rpc Configure(ConfigureRequest) returns (ConfigureResponse);
  rpc Restart(RestartRequest) returns (RestartResponse);
  rpc Reset(ResetRequest) returns (ResetResponse);
  rpc TurnOn(OnRequest) returns (OnResponse);
  rpc TurnOff(OffRequest) returns (OffResponse);
}
-----------------

[source,protobuf]
-----------------
# shared.proto
message GenericDeviceResponse {
  string deviceId = 1;
  int32 return_code = 2;
  repeated Diagnostic msg = 3; // optional diagnostics
}
-----------------

They are:

[source,protobuf]
-----------------
# shared.proto
message HealthCheckRequest {}
message HealthCheckResponse {
  int32 status = 1;
}
-----------------

- *HealthCheck:* A health check at the resource level to determine if the
resource is operating as intended.  The health check is used from an operational
standpoint to allow site operators and merge the ability to ensure the
liveliness of a resource.  The return of an integer allows HealthChecks
to return conditions based on the state depending on how a driver would
like to implement the functionality.

[source,protobuf]
-----------------
# shared.proto
message SetupRequest {
  string deviceId = 1;
  string data = 2;
}
message SetupResponse {
  GenericDeviceResponse response = 1;
}
-----------------

- *Setup:* The setup request takes in a uuid (deviceId) and the JSON
data (data).  It is left up to the driver developer to associate JSON
keys with the desired implementation.

[source,python3]
-----------------
# drivers.py
# called by configure and setup to implement certain functionality fo the droid
def droid_settings(dev_uuid, response, input_data) -> \
        Union[shared_pb2.SetupResponse, shared_pb2.ConfigureResponse]:
    # parse all the inputs
    config = json.loads(input_data)
    apks = config.get('apks', [])
    fi_copy_host = config.get('pull', [])
    fi_copy_droid = config.get('push', [])
    shell_cmds = config.get('shell', [])
    ordered_cmds = config.get('ordered', [])
-----------------

In the example above, from drivers.py in the drivers/Android/drivers directory,
the implementation only accepts `apks`, `pull`, `push`, `shell`, and `ordered`
as JSON keys which will be interpreted by the driver.  For each key, this function
then calls another function which does the actual implementation.  The response
returned by setup is a `GenericDeviceResponse`.  The response in the case of
setup and configure is an array of `Diagnostic` messages which contain the output
and return codes for each action taken by the driver.  The results of those
actions is left up to the experimenter to verify the correctness (such as a
shell command returning a *0* for exit code).

[source,protobuf]
-----------------
# shared.proto
message ConfigureRequest {
  string deviceId = 1;
  string data = 2;
}
message ConfigureResponse {
  GenericDeviceResponse response = 1;
}
-----------------

- *Configure:* Configure is identical to Setup in terms of inputs and outputs,
in fact, in our Android python driver example above, the `droid_settings`
function actually returns either a `SetupResponse` or a `ConfigureResponse`.
This may not be the case for all resources, some resources may require an initial
setup phase where the operations are distinct from configuration.  From an
input/output standpoint however, they take in a uuid and JSON blob and return
a `GenericDeviceResponse`.


- *Restart:* The `RestartRequest` takes in a uuid, JSON blob and transitions
the resource back to its current state by power-cycling.

- *Reset:* The `ResetRequest` takes in a uuid, JSON blob.  A reset wipes all
setup and configurations from the resource and moves it into a clean `ON` state.

- *TurnOn:* The `OnRequest` takes in a uuid, JSON blob from the `OFF` state
to the `ON` state.  In the `ON` state, the resource should have no configuration,
so unlike restart, turning off and on the the resource will cause the device to
be wiped, similar to a reset request.

- *TurnOff:* The `OffRequest` takes in a uuid, JSON blob and transitions
the resource to a powered-off state.


== The Upstream Commander API

